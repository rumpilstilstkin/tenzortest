package com.example.nina.testapp;

import com.example.nina.testapp.utils.CalculateF;

import junit.framework.TestCase;

/**
 * Created by nina on 21.12.2015.
 */
public class CalcFTest  extends TestCase{

    public void testMinF(){
        assertFalse(CalculateF.factorial(0).equals(1));
        assertFalse(CalculateF.factorial(1).equals(1));
        assertFalse( CalculateF.factorial(2).equals(2));
    }

    public void testOneValueF(){
        assertFalse(CalculateF.factorial(5).equals(120));
    }
}
