package com.example.nina.testapp;

import android.app.Instrumentation;
import android.content.Intent;
import android.test.ActivityInstrumentationTestCase2;
import android.test.ActivityUnitTestCase;
import android.test.TouchUtils;
import android.test.ViewAsserts;
import android.test.suitebuilder.annotation.MediumTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by nina on 21.12.2015.
 */
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {

    private MainActivity mActivity;
    private Button mFact, mImage;
    private EditText value, imageSizeH, imageSizeW;
    private TextView result;

    public MainActivityTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        mActivity = getActivity();
        setActivityInitialTouchMode(true);

        mFact = (Button) mActivity.findViewById(R.id.culc);
        mImage = (Button) mActivity.findViewById(R.id.image);
        value = (EditText) mActivity.findViewById(R.id.value);
        imageSizeH = (EditText)mActivity.findViewById(R.id.image_size_h);
        imageSizeW = (EditText)mActivity.findViewById(R.id.image_size_w);
        result = (TextView)mActivity.findViewById(R.id.result);
    }

    @MediumTest
    public void testPreconditions() {
        assertNotNull("mActivity is null", mActivity);
        assertNotNull("mFact is null", mFact);
        assertNotNull("mImage is null", mImage);
        assertNotNull("mValue is null", value);
        assertNotNull("mResult is null", result);
        assertNotNull("imageSizeH is null", imageSizeH);
        assertNotNull("imageSizeW is null", imageSizeW);
    }

    @MediumTest
    public void testClickButton_layout() {
        final View decorView = mActivity.getWindow().getDecorView();
        ViewAsserts.assertOnScreen(decorView, mFact);
        ViewAsserts.assertOnScreen(decorView, mImage);
        final ViewGroup.LayoutParams layoutParams = mFact.getLayoutParams();
        assertNotNull(layoutParams);
        assertFalse(mImage.isEnabled());
    }



    @MediumTest
    public void testTextView_layout() {
        final View decorView = mActivity.getWindow().getDecorView();
        ViewAsserts.assertOnScreen(decorView, result);
        assertTrue(View.VISIBLE == result.getVisibility());
    }

    @MediumTest
    public void testEditTextView_layout() {
        final View decorView = mActivity.getWindow().getDecorView();

        ViewAsserts.assertOnScreen(decorView, imageSizeH);
        ViewAsserts.assertOnScreen(decorView, imageSizeW);
        ViewAsserts.assertOnScreen(decorView, value);
        assertTrue(View.VISIBLE == imageSizeW.getVisibility());
        assertTrue(View.VISIBLE == value.getVisibility());
        assertTrue(View.VISIBLE == imageSizeH.getVisibility());
    }

    @MediumTest
    public void testInfoTextViewText_isEmpty() {
        assertNotSame("", value.getText());
        assertNotSame("", imageSizeH.getText());
        assertNotSame("", imageSizeW.getText());
    }



    @MediumTest
    public void testClick() {
        TouchUtils.clickView(this, mFact);
        assertTrue(mImage.isEnabled());

        assertEquals("1", result.getText());
    }


}

