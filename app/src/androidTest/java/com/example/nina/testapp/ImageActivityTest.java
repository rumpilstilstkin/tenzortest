package com.example.nina.testapp;

import android.test.ActivityInstrumentationTestCase2;
import android.test.ViewAsserts;
import android.test.suitebuilder.annotation.MediumTest;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Created by nina on 21.12.2015.
 */
public class ImageActivityTest extends ActivityInstrumentationTestCase2<ImageActivity> {

    private ImageActivity mActivity;
    private ImageView image;
    private ProgressBar progress;

    public ImageActivityTest() {
        super(ImageActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        mActivity = getActivity();

        image = (ImageView) mActivity.findViewById(R.id.image);
        progress = (ProgressBar) mActivity.findViewById(R.id.progress);
    }

    @MediumTest
    public void testPreconditions() {
        assertNotNull("mActivity is null", mActivity);
        assertNotNull("mImage is null", image);
        assertNotNull("mProgress is null", progress);
    }

    @MediumTest
    public void testClickButton_layout() {
        final View decorView = mActivity.getWindow().getDecorView();
        ViewAsserts.assertOnScreen(decorView, image);
        ViewAsserts.assertOnScreen(decorView, progress);
        assertFalse(progress.getVisibility() == View.GONE);
    }
}
