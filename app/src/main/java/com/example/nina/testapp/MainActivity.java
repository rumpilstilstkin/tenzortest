package com.example.nina.testapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.nina.testapp.utils.CalculateF;
import com.example.nina.testapp.utils.InputFilterMinMax;

public class MainActivity extends AppCompatActivity {
    private EditText value, imageSizeH, imageSizeW;
    private TextView result;
    private Button image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        value = (EditText)findViewById(R.id.value);
        value.setFilters(new InputFilter[]{ new InputFilterMinMax("0", "500")});
        result = (TextView)findViewById(R.id.result);
        image = (Button)findViewById(R.id.image);
        imageSizeW = (EditText)findViewById(R.id.image_size_w);
        imageSizeH = (EditText)findViewById(R.id.image_size_h);
        imageSizeW.setFilters(new InputFilter[]{ new InputFilterMinMax("0", "1920")});
        imageSizeH.setFilters(new InputFilter[]{ new InputFilterMinMax("0", "1080")});

    }

    public void onF(View view){
        int v =  value.getText().toString().isEmpty()? 0 : Integer.parseInt(value.getText().toString());
        result.setText(CalculateF.factorial(v).toString());
        image.setEnabled(true);


    }

    public void onImage(View view){
        int size_w = imageSizeW.getText().toString().isEmpty()? 1920 : Integer.parseInt(imageSizeW.getText().toString());
        int size_h = imageSizeH.getText().toString().isEmpty()? 1080 : Integer.parseInt(imageSizeH.getText().toString());
        Intent intent = new Intent(this, ImageActivity.class);
        intent.putExtra(ImageActivity.SIZE_KEY_W, size_w);
        intent.putExtra(ImageActivity.SIZE_KEY_H, size_h);
        startActivity(intent);

    }
}
