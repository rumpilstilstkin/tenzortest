package com.example.nina.testapp;

import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

/**
 * Created by nina on 21.12.2015.
 */
public class ImageActivity extends AppCompatActivity {
    public static String SIZE_KEY_W = "size_key_w";
    public static String SIZE_KEY_H = "size_key_h";

    private static String IMAGE_KEY = "image_key";
    private int size_w = 1920;
    private int size_h = 1080;
    private ImageView image;
    private ProgressBar progress;
    private Bitmap bmp = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_layout);




        size_w = getIntent().getIntExtra(SIZE_KEY_W, 1920);
        size_h = getIntent().getIntExtra(SIZE_KEY_H, 1080);
        progress = (ProgressBar)findViewById(R.id.progress);
        image = (ImageView)findViewById(R.id.image);
        byte[] byteArray = getIntent().getByteArrayExtra(IMAGE_KEY);
        if(byteArray != null){
            bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            image.setImageBitmap(bmp);
            progress.setVisibility(View.GONE);
        }else {
            new DownloadImageTask()
                    .execute("http://cdn.oboi7.com/cd83c2ca6269fe5b6607c68c3dfb9b73064e1fdb/zakat-oblaka-pejzazhi.jpg");
        }

    }



    @Override
    protected void onSaveInstanceState(Bundle outState) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        outState.putByteArray(IMAGE_KEY, byteArray);
        super.onSaveInstanceState(outState);

    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {


        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if(mIcon11 != null){
                mIcon11 = resize(mIcon11);
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            progress.setVisibility(View.GONE);
            if(result == null){
                Toast.makeText(ImageActivity.this, "Ошибка передачи данных", Toast.LENGTH_SHORT).show();
                image.setImageResource(R.mipmap.ic_launcher);
            }else {
                bmp = result;
                image.setImageBitmap(result);
            }
        }

        private Bitmap resize(Bitmap b){
            return Bitmap.createScaledBitmap(b, size_w, size_h, true);
        }
    }
}
