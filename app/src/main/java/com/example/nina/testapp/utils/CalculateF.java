package com.example.nina.testapp.utils;

import java.math.BigInteger;
import java.util.HashMap;

/**
 * Created by nina on 21.12.2015.
 */
public class CalculateF {

    static HashMap<Integer,BigInteger> cache = new HashMap<Integer,BigInteger>();

    public static BigInteger factorial(int n)
    {
        BigInteger ret;

        if (n == 0) return BigInteger.ONE;
        if (null != (ret = cache.get(n))) return ret;
        ret = BigInteger.valueOf(n).multiply(factorial(n-1));
        cache.put(n, ret);
        return ret;

       }
}

